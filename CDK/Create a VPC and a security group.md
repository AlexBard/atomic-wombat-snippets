# Create a VPC and a security group

## Prerequisites

You need to install the AWS CDK (doc [here](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html#getting_started_install)) and configure it properly.

## Snippet

```py
from aws_cdk import aws_ec2 as ec2

vpc = ec2.Vpc(
    self,
    "MyAwesomeVPC",
    vpc_name="my-awesome-vpc",
    ip_addresses=ec2.IpAddresses.cidr("10.0.0.0/16"),
    max_azs=3,
    nat_gateways=3,
    subnet_configuration=[
        ec2.SubnetConfiguration(
            name="my-subnet-public",
            subnet_type=ec2.SubnetType.PUBLIC,
            cidr_mask=20,
        ),
        ec2.SubnetConfiguration(
            name="my-subnet-private",
            subnet_type=ec2.SubnetType.PRIVATE_WITH_EGRESS,
            cidr_mask=20,
        ),
    ],
    create_internet_gateway=True,
)

my_security_group = ec2.SecurityGroup(
    self,
    "MyAwesomeSecurityGroup",
    security_group_name="my-sg",
    vpc=vpc,
    allow_all_outbound=True,
)
my_security_group.add_ingress_rule(
    peer=ec2.Peer.ipv4(vpc.vpc_cidr_block),
    connection=ec2.Port.tcp(443),
)
```

## Pitfalls

Some cloud components (like NAT Gateway) incur costs. Make sure you understand what you are creating and how much it will cost you. You can always immediately delete your CDK stack with `cdk destroy "*"`. Check AWS Budgets to setup budget alerts.