# Add Firebase Authentication

## Prerequisites

You need to install firebase_admin in your project (doc [here](https://pypi.org/project/firebase-admin/)).


## Snipet

```py
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from firebase_admin import auth, messaging


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def auth_depends(token: str = Depends(oauth2_scheme)):
    try:
        verification = auth.verify_id_token(token)
        return verification
    except ExpiredIdTokenError as exc:
        logger.warning("Token expired")
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)




@app.get("/my_route")
async def my_route(verification=Depends(auth_depends)):
    # Follow with your logic. `verification` will contain basic information like user id.
    uid = verification["user_id"]
```

# Tips
Use the `Bearer` embedded in `Authorization` header to transmit the auth token from the client.